//
//  ViewController.swift
//  ProtocolValTransfer
//
//  Created by LiJinxu on 16/5/4.
//  Copyright © 2016年 LiJinxu-NEU. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var receiveLabel: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func sendBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("toBViewController", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "toBViewController"){
            let bVC : BViewController = segue.destinationViewController as! BViewController
            bVC.tempString = self.textField.text
            bVC.delegate = self
        }
    }
    
}

extension ViewController:SendMessageDelegate{
    
    func sendWord(message: String) {
        receiveLabel.text = message
    }
}
