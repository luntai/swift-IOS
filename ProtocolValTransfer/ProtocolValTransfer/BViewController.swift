//
//  BViewController.swift
//  ProtocolValTransfer
//
//  Created by LiJinxu on 16/5/4.
//  Copyright © 2016年 LiJinxu-NEU. All rights reserved.
//

import Foundation
import UIKit

protocol SendMessageDelegate {
    func sendWord(message: String)
}

class BViewController: UIViewController{

    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var receiveLabel: UILabel!
    
    @IBAction func sendBtnAction(sender: AnyObject) {
        if(delegate != nil){
            self.delegate?.sendWord(textField.text!)
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    var delegate:SendMessageDelegate?
    
    var tempString:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.receiveLabel.text = tempString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
